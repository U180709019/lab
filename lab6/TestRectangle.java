

public class TestRectangle {
    public static void main(String[] args) {

        // Point p = new Point(3,7);
        Rectangle r = new Rectangle(5,6,new Point(3,7));
        System.out.println("area = " + r.area());

        Point[] corners = r.corners();
        for (Point item : corners){
            System.out.println("x = " + item.xCoord + " y = "+ item.yCoord);
        }

        Rectangle r2 = new Rectangle(7,9,new Point(3,10));
        System.out.println("area = " + r2.area());
        System.out.println("perimeter " + r2.perimeter());

    }

}