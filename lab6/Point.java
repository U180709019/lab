public class Point {
    int xCoord = 1;
    int yCoord = 1;

    public Point() {

    }

    public Point(int xy) {
        this.xCoord = xy;
        this.yCoord = xy;
    }

    public Point(int xCoord, int yCoord) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }

    public double distance(Point p){
        return Math.sqrt((xCoord-p.xCoord)*(xCoord-p.xCoord)+(yCoord-p.yCoord)*(yCoord-p.yCoord));
    }
}
