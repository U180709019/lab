public class GDCRec {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);

        int greater = a > b ? a : b;
        int smaller = a < b ? a : b;

        System.out.println("GDC : " + gdc(greater,smaller));
    }

    public static int gdc(int greater, int smaller){
        if (smaller == 0)
            return greater;

        return  gdc(smaller,greater % smaller);
    }
}
